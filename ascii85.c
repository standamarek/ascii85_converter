/** @file ascii85.c */

/**
 * Program na prevod znaku do kodovani ascii85.
 *
 * @author      Stanislav Marek <394122@mail.muni.cz>
 * @version     1.0
 * @since       2013-03-08
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Prototypy funkci:
 * @see toAscii85
 * @see zakoduj
 * @see dekoduj
 */
void toAscii85(int, unsigned char *);
void zakoduj(unsigned int, unsigned char *);
void dekoduj(unsigned int, unsigned char *);

/**
 * Prevadi 4 znaky do ascii85 a vypisuje.
 * @param pozice udava pozici od ktere z pole vybirame 4 znaky
 * @param znaky  ukazatel na pole se znaky
 */
void toAscii85(int pozice, unsigned char * znaky){
    /**
     * Slouzi k ulozeni 4 znaku "za sebe".
     */
    long int byteZnaku = znaky[pozice];
    /**
     * Zde poskladame znaky binarne za sebe.
     */
    for(int i = 1; i < 4; ++i){
        byteZnaku <<= 8;
        byteZnaku += znaky[pozice + i];
    }
    /**
     * Pole pro ulozeni znaku zakodovanych v ascii85.
     */
    unsigned char zakodovano[5];
    /**
     * Prevadime nas byte zpatky na znaky.
     *
     * Ukladame do pole od nejvyssiho indexu po nejnizsi.
     */
    for(int i = 4; i >= 0; i--){
        zakodovano[i] = ((byteZnaku % 85) + 33);
        byteZnaku /= 85;
    }
    /**
     * Vypiseme prevedene znaky.
     */
    for(int i = 0; i < 5; ++i){
        printf("%c", zakodovano[i]);
    }
}

/**
 * Fuknce ktera obstarava zakodovani do ascii85, vola @see toAscii85.
 * @param pocet pocet nactenych znaku
 * @param znaky ukazatel na pole znaku
 */
void zakoduj(unsigned int pocet, unsigned char * znaky){
    /**
     * Vypocitame si pocet nul.
     */
    unsigned int pocetNul = (pocet % 4);
    if(pocetNul){
        pocetNul = 4 - pocetNul;
    }
    /**
     * Doplnime nuly abychom meli pocet znaku delitelny 4.
     */
    for(unsigned int i = 0; i < pocetNul && pocetNul != 4; ++i){
        znaky[pocet + i] = '\0';
    }
    /**
     * Kazdou ctverici znaku prevedeme do ascii85 a vypiseme pomoci @see toAscii85.
     */
    for(unsigned int i = 0; i < ((pocet + pocetNul) / 4); ++i){
        toAscii85(4*i, znaky);
    }
}

/**
 * Funkce na dekodovani z ascii85 a vypsani.
 * @param pocet pocet nactenych znaku
 * @param znaky ukazatel na pole znaku
 */
void dekoduj(unsigned int pocet, unsigned char * znaky){
    /**
     * Rozdelime znaky na petice.
     */
    for(unsigned int i = 0; i < (pocet/5); ++i){
        long int byteZnaku = 0;
        /**
         * Vypocitame si binarni hodnotu ctverice znaku ktere hledame.
         */
        for(int j = 0; j < 4; ++j){
            byteZnaku += znaky[(5 * i) + j] - 33;
            byteZnaku *= 85;
        }

        byteZnaku += (znaky[(5 * i) + 4] - 33);
        /**
         * Postupne z binarni hodnoty ctverice znaku zjistime jednotlive znaky.
         */
        unsigned char rozkodovano[4];
        for(int j = 3; j >= 0; --j){
            rozkodovano[j] = byteZnaku % 256;
            byteZnaku /= 256;
        }
        /**
         * Vypiseme rozkodovanou ctverici znaku.
         */
        for(int j = 0; j < 4; ++j){
            printf("%c", rozkodovano[j]);
        }
    }
}
/**
 * Funkce main, bez prepinace koduje do ascii 85, s prepinacem -d dekoduje z ascii85.
 * @param argc pocet zadanych argumentu
 * @param argv pole retezcu jednotlivych argumentu
 * @return pouzivam knihovni konstanty, vraci 0 pri uspesnem pruchodu programu a 1 pokud nastane chyba.
 */
int main(int argc, char** argv){
    /**
     * Limit omezuje pocet nacitanych znaku.
     */
    unsigned int limit = 40;
    /**
     * S prepinacem potrebujeme limit vetsi.
     */
    if(argc == 2 && !strcmp(argv[1], "-d")){
        limit = 50;
    }
    /**
     * Pole do ktereho budeme ukladat nacitane znaky.
     */
    unsigned char znak[limit];
    /**
     * Pocet nactenych znaku.
     */
    unsigned int nactenych = 0;
    /**
     * Pomocna promenna do ktere nacitame znak pred kontrolou a ulozenim do pole nactenych znaku.
     */
    int nactenyZnak = getchar();
    /**
     * Nacitame maximalne @see limit znaku do konce radku nebo konce souboru.
     */
    while(nactenyZnak != EOF && nactenyZnak != '\n' && nactenych != limit){
        znak[nactenych] = nactenyZnak;
        nactenych++;
        nactenyZnak = getchar();
    }
    /**
     * Podle toho jaky je nastaveny limit rozhodneme, jestli kodovat nebo dekodovat.
     */
    switch(limit){
    case 50:
        dekoduj(nactenych, znak);
        break;
    case 40:
        zakoduj(nactenych, znak);
    }
    printf("\n");
    return EXIT_SUCCESS;
}
